class CreateHints < ActiveRecord::Migration[5.0]
  def change
    create_table :hints do |t|
      t.integer :source, null: false
      t.string :start_node, null: false
      t.string :end_node, null: false
      t.string :start_time, null: false
      t.string :end_time, null: false
      t.timestamps
    end
  end
end
