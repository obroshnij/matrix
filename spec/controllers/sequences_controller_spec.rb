require 'rails_helper'

RSpec.describe SequencesController, type: :controller do
  render_views

  describe 'POST #create' do
    before do
      post :create
    end

    it 'expect response redirect' do
      expect(response).to redirect_to parse_sequences_path
    end
  end

  describe 'GET #parse' do
    before do
      get :parse
    end

    it 'response status ok' do
      expect(response.status).to eq 200
    end

    it 'contains link' do
      expect(response.body).to match('Parse Downloaded Data')
    end
  end

  describe 'POST #perform_parse' do
    before do
      post :perform_parse
    end

    it 'expect redirect' do
      expect(response).to redirect_to preview_sequences_path
    end
  end

  describe 'GET #parse' do
    before do
      get :preview
    end

    it 'response status ok' do
      expect(response.status).to eq 200
    end

    it 'contains link' do
      expect(response.body).to match('Post parsed Data')
    end
  end

  describe 'POST #perform_parse' do
    before do
      post :post_data
    end

    it 'expect redirect' do
      expect(response).to redirect_to root_path
    end
  end
end
