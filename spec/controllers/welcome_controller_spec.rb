require 'rails_helper'

RSpec.describe WelcomeController, type: :controller do
  render_views

  describe 'GET #index' do
    before do
      get :index
    end

    it 'expect response ok' do
      expect(response.status).to eq 200
    end

    it 'contains link' do
      expect(response.body).to match('Initiate rescuing sequence')
    end
  end
end
