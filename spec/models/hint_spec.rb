require 'rails_helper'

RSpec.describe Hint, type: :model do
  let(:download_sources) { Hint.download_sources }
  let(:parse_downloaded_data) { Hint.parse_downloaded_data }
  let(:send_all_data) { Hint.send_all_data }

  context 'model validations' do
    it { should validate_presence_of(:source) }
    it { should validate_presence_of(:start_node) }
    it { should validate_presence_of(:end_node) }
    it { should validate_presence_of(:start_time) }
    it { should validate_presence_of(:end_time) }
  end

  describe 'download_sources' do
    before do
      download_sources
    end

    it 'downloads sources' do
      expect(File.directory?(Rails.root.join('tmp', 'downloads', 'sentinels'))).to be_truthy
      expect(File.directory?(Rails.root.join('tmp', 'downloads', 'loopholes'))).to be_truthy
      expect(File.directory?(Rails.root.join('tmp', 'downloads', 'sniffers'))).to be_truthy
    end
  end

  describe 'parse_downloaded_data' do
    before do
      @hints = Hint.count
      parse_downloaded_data
    end

    it 'parses and includes the data into db' do
      expect(Hint.count > @hints).to be_truthy
    end
  end

  describe 'send_all_data' do
    before do
      @hints = Hint.count
      send_all_data
    end

    it 'parses and includes the data into db' do
      expect(Hint.count.zero?).to be_truthy
    end
  end

  describe 'clean directory' do
    before do
      Hint.clean_working_directory
    end

    it 'parses and includes the data into db' do
      expect(File.directory?(Rails.root.join('tmp', 'downloads', 'sentinels'))).to be_falsey
      expect(File.directory?(Rails.root.join('tmp', 'downloads', 'loopholes'))).to be_falsey
      expect(File.directory?(Rails.root.join('tmp', 'downloads', 'sniffers'))).to be_falsey
    end
  end
end
