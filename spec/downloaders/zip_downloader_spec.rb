require 'rails_helper'

describe 'Zip downloader', type: :feature do
  let(:sentinels_downloader){ ZipDownloader.new('http://challenge.distribusion.com/the_one/routes', { passphrase: "Kans4s-i$-g01ng-by3-bye", source: 'sentinels' })}

  describe 'basic actions' do
    it 'downloads data with unzip action' do
      sentinels_downloader.unzip
      expect(File.directory?(Rails.root.join('tmp', 'downloads', 'sentinels'))).to be_truthy
    end
  end
end
