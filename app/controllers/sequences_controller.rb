class SequencesController < ApplicationController
  def create
    if Hint.download_sources
      respond_to do |format|
        format.html { redirect_to parse_sequences_path }
      end
    end
  end

  def parse
    respond_to do |format|
      format.html
    end
  end

  def perform_parse
    if Hint.parse_downloaded_data
      respond_to do |format|
        format.html { redirect_to preview_sequences_path }
      end
    end
  end

  def preview
    @hints = Hint.all
    respond_to do |format|
      format.html
    end
  end

  def post_data
    if Hint.send_all_data
      respond_to do |format|
        format.html { redirect_to root_path }
      end
    end
  end
end
