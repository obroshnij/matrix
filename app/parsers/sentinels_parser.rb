class SentinelsParser < AbstractParser
  attr_reader :parsed_objects

  SENTINELS_PATH = Rails.root.join('tmp', 'downloads', 'sentinels')

  def initialize
    @routes = read_csv(SENTINELS_PATH.join('routes.csv'))
    @parsed_objects = []
  end

  def source
    'sentinels'
  end

  def start_node(item)
    item[:start_node]['node']
  end

  def end_node(item)
    item[:end_node]['node']
  end

  def start_time(item)
    Time.parse(item[:start_node]['time']).utc.iso8601
  end

  def end_time(item)
    Time.parse(item[:end_node]['time']).utc.iso8601
  end

  def parse
    route_ids = @routes.values_at('route_id').flatten.uniq.map &:to_i
    route_ids.map do |id|
      route_nodes = @routes.find_all {|route| route['route_id'] == id.to_s }
      start_node = route_nodes.find {|route| route['index'] == route_nodes.pluck('index').map(&:to_i).min.to_s }
      end_node = route_nodes.find {|route| route['index'] == route_nodes.pluck('index').map(&:to_i).max.to_s }
      @parsed_objects << build_object({start_node: start_node, end_node: end_node})
    end
  end
end
