class SniffersParser < AbstractParser
  attr_reader :parsed_objects

  SNIFFERS_PATH = Rails.root.join('tmp', 'downloads', 'sniffers')

  def initialize
    @routes = read_csv(SNIFFERS_PATH.join('routes.csv'))
    @node_times = read_csv(SNIFFERS_PATH.join('node_times.csv'))
    @sequences = read_csv(SNIFFERS_PATH.join('sequences.csv'))
    @parsed_objects = []
  end

  def source
    'sniffers'
  end

  def start_node(item)
    item[:node_time]['start_node']
  end

  def end_node(item)
    item[:node_time]['end_node']
  end

  def start_time(item)
    Time.parse(item[:route]['time'] + item[:route]['time_zone']).utc.iso8601
  end

  def end_time(item)
    time_in_ms = Time.parse(item[:route]['time'] + item[:route]['time_zone']).to_i * 1000
    mls_to_add = item[:node_time]['duration_in_milliseconds'].to_i
    Time.at((time_in_ms + mls_to_add) / 1000).utc.iso8601
  end

  def parse
    @sequences.each do |sequence|
      route = @routes.find { |r| r['route_id'] == sequence['route_id'] }
      node_time = @node_times.find { |n| n['node_time_id'] == sequence['node_time_id'] }
      next unless node_time && route
      @parsed_objects << build_object(route: route, node_time: node_time)
    end
  end
end
