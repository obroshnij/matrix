class LoopholesParser < AbstractParser
  attr_reader :parsed_objects

  LOOPHOLES_PATH = Rails.root.join('tmp', 'downloads', 'loopholes')

  def initialize
    @routes = read_json(LOOPHOLES_PATH.join('routes.json'))
    @node_pairs = read_json(LOOPHOLES_PATH.join('node_pairs.json'))
    @parsed_objects = []
  end

  def source
    'loopholes'
  end

  def start_node(item)
    node = @node_pairs['node_pairs'].find { |pair| pair['id'] == item['node_pair_id'] }
    node['start_node'] if node
  end

  def end_node(item)
    node = @node_pairs['node_pairs'].find { |pair| pair['id'] == item['node_pair_id'] }
    node['end_node'] if node
  end

  def start_time(item)
    Time.parse(item['start_time']).utc.iso8601
  end

  def end_time(item)
    Time.parse(item['end_time']).utc.iso8601
  end

  def parse
    @routes['routes'].each do |route|
      next unless start_node(route)
      @parsed_objects << build_object(route)
    end
  end
end
