class AbstractParser
  def build_object(item)
    OpenStruct.new(source: source, start_node: start_node(item), end_node: end_node(item), start_time: start_time(item), end_time: end_time(item))
  end

  protected

  def read_json(path)
    JSON.parse(File.read(path))
  end

  def read_csv(path)
    CSV.parse(File.open(path).read.gsub(/[" ]/, ''), headers: true)
  end
end
