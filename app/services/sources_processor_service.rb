class SourcesProcessorService
  LOCATION = 'http://challenge.distribusion.com/the_one/routes'.freeze
  SOURCE_LIST = %w(sentinels sniffers loopholes)

  def initialize
    @credentials = Requestors::CredentialsRequestor.new
  end

  def download_files
    SOURCE_LIST.map { |source| download(source) }
  end

  def download(source)
    zip = ZipDownloader.new(LOCATION, { passphrase: @credentials.passphrase, source: source })
    zip.unzip
  end
end
