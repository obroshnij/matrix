class Hint < ApplicationRecord
  extend Files
  include DataSubmit

  validates :source, presence: true
  validates :start_node, presence: true
  validates :end_node, presence: true
  validates :start_time, presence: true
  validates :end_time, presence: true

  enum source: [:sentinels, :sniffers, :loopholes]

  def self.download_sources
    self.clean_working_directory
    self.create_working_directory
    start_sequence = SourcesProcessorService.new
    start_sequence.download_files
    true
  end

  def self.parse_downloaded_data
    %w(SniffersParser SentinelsParser LoopholesParser).map do |parser|
      current_parser = parser.constantize.new
      current_parser.parse
      current_parser.parsed_objects.map do |item|
        Hint.create(item.to_h)
      end
    end
    self.clean_working_directory
    true
  end

  def self.send_all_data
    pass = Requestors::CredentialsRequestor.new.passphrase
    Hint.all.map { |hint| hint.submit(pass) }
  end
end
