module Files
  def clean_working_directory
    FileUtils.rm_rf(Rails.root.join('tmp', 'downloads'))
  end

  def create_working_directory
    FileUtils.mkdir(Rails.root.join('tmp', 'downloads'))
  end
end
