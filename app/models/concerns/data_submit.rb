module DataSubmit
  SUBMIT_PATH = 'http://challenge.distribusion.com/the_one/routes'.freeze

  def post_data_to_server(data)
    HTTParty.post(SUBMIT_PATH, query: data, headers: { 'Content-Type' => 'application/json' })
    delete
  end

  def submit(pass)
    post_data_to_server(
      passphrase: pass,
      source: source,
      start_node: start_node,
      end_node: end_node,
      start_time: start_time[0...-1],
      end_time: end_time[0...-1]
    )
  end
end
