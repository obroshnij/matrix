require 'zipruby'

class ZipDownloader
  FILE_STORAGE_PATH = Rails.root.join('tmp', 'downloads')

  def initialize(url, params)
    @params = params
    @url = url
  end

  def unzip
    Zip::Archive.open_buffer(buffered_response.body) do |archive_contents|
      archive_contents.each do |content_item|
        if content_item.directory?
          create_directory(content_item.name)
        else
          create_file(File.dirname(content_item.name), content_item)
        end
      end
    end
  end

  def buffered_response
    Requestors::ZipRequestor.new(@url, @params).response
  end

  private

  def create_file(dirname, file_object)
    create_directory(dirname) unless File.exist?(dirname)
    write_file(FILE_STORAGE_PATH.join(file_object.name), file_object.read)
  end

  def create_directory(name)
    FileUtils.mkdir_p(FILE_STORAGE_PATH.join(name))
  end

  def write_file(file_path, content)
    open(file_path, 'wb') do |f|
      f << content
    end
  end
end
