class Requestors::BaseRequestor
  include HTTParty

  protected

  def get_json_request(url, query=nil)
    HTTParty.get(url, query: query, headers: { 'Accept' => 'application/json' })
  end

  def get_zip_request(url, query=nil)
    HTTParty.get(url, query: query, headers: { 'Accept' => 'application/zip', 'Content-Type' =>'application/zip' })
  end
end