class Requestors::ZipRequestor < Requestors::BaseRequestor
  attr_reader :response

  def initialize(location, params)
    @location = location
    @params = params
  end

  def response
    get_zip_request(@location, @params)
  end
end
