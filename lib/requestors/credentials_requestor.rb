class Requestors::CredentialsRequestor < Requestors::BaseRequestor
  ENTRY_POINT = 'http://challenge.distribusion.com/the_one'.freeze

  attr_reader :location, :passphrase

  def initialize
    request_creds
    set_attributes
  end

  private

  def request_creds
    @response = get_json_request(ENTRY_POINT)
  end

  def set_attributes
    @location = as_json['pills']['red']['location']
    @passphrase = as_json['pills']['red']['passphrase']
  end

  def as_json
    JSON.parse(@response.body)
  end
end
