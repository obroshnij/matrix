Rails.application.routes.draw do
  root 'welcome#index'

  resources :sequences, only: [:create, :parse, :preview, :perform_parse] do
    collection do
      get :parse
      get :preview
      post :post_data
      post :perform_parse
    end
  end
end
